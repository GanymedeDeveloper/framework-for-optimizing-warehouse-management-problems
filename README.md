# A framework for Optimizing Common Warehouse Optimization problems

![](Images/MainMenu.png)

#### Abstract

With the recent rally of E-commerce worldwide in the last couple of years, warehouse management are competing to optimize every possible part of work, from packing and shipping to receiving and order picking. Order picking is the costliest one between all warehouse operations and based on some estimates it can take up to 55% of warehous operating cost. Order picking nowadays is still mostly led by humans therefore the process is exposed to the inefficiency that is caused by motion waste (the unnecessary movements within the warehouse) that humans have. One way to improve order picking and reduce motion waste, is to optimize two common warehouse management problems, namely, Pick Path Optimization and Order Batching. Another way to improve order picking is to use collaborative robots that perform the whole picking process instead of humans. Finding optimal routes for the robots to collect the orders is also a warehouse
optimization problem. Finding optimal solutions for these three NP-hard problems on the various warehouse layouts that business and firms has is a challenge. In this project a framework for optimizing warehouse optimization problems is proposed and implemented. Developers using the framework can quickly create their desired layouts, store them in a database using a user-friendly interface, quickly integrate their own solutions (algorithms) for the above mentioned problems and switch between these solutions on the various layouts. Comparing the results of these solutions on different warehouse layouts will help developers to identify optimal ones that suit developer’s needs. The framework can also be used to quickly make applications that offer solutions
to these three optimization problems.

#### Goal

The main goal of this framework is to offer developers a simulation enviroment where different solutions for problems like Pick Path Optimization, Order Batching and Robot Routing can be easily integrated, tested and compared on different warehouse layouts that firms have to identify optimal ones.

#### Designing Warehouse layouts

Developers using the framework can easily replicate their desired layouts.

![](Images/Layouts.png)

#### Pick Path Optimization

Pick Path Optimization is the process of finding the fastest way to navigate the warehouse to collect an order.

![](Images/ClickingPathButton.jpg)

#### Order Batching problem

The process of combining multiple orders to be collected at the same time instead of collecting them individually.

![](Images/OrderBatchingBatchButton.jpg)

#### Robots Routing problem

The Process of finding optimal routes for the robots to collect an Order

![](Images/RobotsScene.png)

