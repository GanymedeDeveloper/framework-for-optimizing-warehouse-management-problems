
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using System.Linq;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static PathNode;
using TMPro;

public class RobotsRoutingManager : MonoBehaviour
{
    #region Attributes
    [SerializeField] private GridVisuals gridVisuals;
    [SerializeField] private InputWindow saveInputWindow;
    [SerializeField] private LayoutInputWindow capacityInputWindow;
    [SerializeField] private SQLiteManager sqLiteManager;
    [SerializeField] private Button capacityButton;
    [SerializeField] private TextMeshProUGUI capacityText;

    [SerializeField] public Button RunRobotButton;
    [SerializeField] public Button AddOrderButton;

    public GameObject robotPrefab;
    public GameObject stationPrefab;
    public Transform instancesParent;
    public RobotSelection robotSelection;
    public int capacity;

    public List<RobotsBrain> robotList = new List<RobotsBrain>();
    public List<Station> stationtList = new List<Station>();

    public HashSet<Vector3> mousePositionsOrders = new HashSet<Vector3>();
    public List<List<Vector3>> orders = new List<List<Vector3>>();

    public CursorPosition cursorPosition;
    
    private int sceneIndex;

    private PathNode draggedNode;
    private GameObject draggedObject;
    private RobotsBrain draggedRobot;
    private Station draggedStation;
    private Grid<PathNode> grid;

    public CollectionStrategies collectionStartegy;
    #endregion

    private void Awake()
    {
        if (ChooseDropDown.mainGrid != null)
        {
            grid = new Grid<PathNode>(ChooseDropDown.mainGrid.GetWidth(), ChooseDropDown.mainGrid.GetHeight(), 10f, (Grid<PathNode> g, int x, int y) => new PathNode(g, x, y));

            for (int i = 0; i < ChooseDropDown.mainGrid.GetWidth(); i++)
            {
                for (int j = 0; j < ChooseDropDown.mainGrid.GetHeight(); j++)
                {
                    PathNode pn = new PathNode(grid, i, j);
                    pn.SetIsItem(ChooseDropDown.mainGrid.GetCell(i, j).isItem);
                    pn.SetIsWalkable(ChooseDropDown.mainGrid.GetCell(i, j).isWalkAble);

                    grid.SetCell(i, j, pn);
                }
            }
        }
        else
        {
            grid = new Grid<PathNode>(20, 10, 10f, (Grid<PathNode> g, int x, int y) => new PathNode(g, x, y));
        }

        RunRobotButton.interactable = false;

    }

    private void Start()
    {
        gridVisuals.SetGrid(grid);
        draggedNode = null;

        foreach (Station s in stationtList)
        {
            grid.GetCell(Mathf.RoundToInt((s.transform.position.x - 5) / 10f), Mathf.RoundToInt((s.transform.position.y - 5) / 10f)).SetStation(true);    
        }

        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        
        RemoveItemsFromGrid();
        capacityButton.interactable = false;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            Vector3 mouseWorldPosition = UtilsClass.GetMouseWorldPosition();

            grid.GetCoordinates(mouseWorldPosition, out int x, out int y);

            PathNode currentNode = grid.GetCell(x, y);
            if (currentNode != null)
            {
                if (currentNode.hasRobot)
                {
                    foreach (RobotsBrain r in robotList)
                    {
                        if (Mathf.RoundToInt((r.transform.position.x - 5) / 10f) == currentNode.x && Mathf.RoundToInt((r.transform.position.y - 5) / 10f) == currentNode.y)
                        {
                            draggedObject = r.gameObject;
                            cursorPosition.SetCursorType(0);
                            break;
                        }
                    }
                    draggedNode = currentNode;
                }
                else if (currentNode.hasStation)
                {
                    foreach (Station s in stationtList)
                    {
                        if (Mathf.RoundToInt((s.transform.position.x - 5) / 10f) == currentNode.x && Mathf.RoundToInt((s.transform.position.y - 5) / 10f) == currentNode.y)
                        {
                            draggedObject = s.gameObject;
                            cursorPosition.SetCursorType(1);
                            break;
                        }
                    }
                    draggedNode = currentNode;
                }
                else
                {
                    draggedNode = null;
                    draggedObject = null;
                    grid.GetCell(x, y).SetIsItem(!grid.GetCell(x, y).isItem);

                    if (!mousePositionsOrders.Contains(grid.GetMouseCoordinates(x, y)))
                    {
                        mousePositionsOrders.Add(grid.GetMouseCoordinates(x, y));
                    }
                    else
                    {
                        mousePositionsOrders.Remove(grid.GetMouseCoordinates(x, y));
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0) && draggedNode != null && draggedObject != null)
        {
            Vector3 mouseWorldPosition = UtilsClass.GetMouseWorldPosition();
            grid.GetCoordinates(mouseWorldPosition, out int x, out int y);
            PathNode currentNode = grid.GetCell(x, y);

            if (!currentNode.hasRobot && !currentNode.hasStation)
            {
                currentNode.hasRobot = draggedNode.hasRobot;
                currentNode.hasStation = draggedNode.hasStation;

                if (currentNode.hasRobot)
                {
                    draggedNode.SetIsWalkable(true);
                    currentNode.SetIsWalkable(false);
                }

                draggedNode.SetRobot(false);
                draggedNode.SetStation(false);

                draggedObject.transform.position = new Vector3(currentNode.x * 10 + 5, currentNode.y * 10 + 5, 0);
            }
            draggedNode = null;
            draggedObject = null;
            cursorPosition.SetCursorType(-1);
        }
        if (Input.GetMouseButtonDown(1))
        {
            Vector3 mouseWorldPosition = UtilsClass.GetMouseWorldPosition();
            grid.GetCoordinates(mouseWorldPosition, out int x, out int y);
            grid.GetCell(x, y).SetIsWalkable(!grid.GetCell(x, y).isWalkAble);
        }

        if (Input.GetMouseButtonDown(2))
        {
            Vector3 mouseWorldPosition = UtilsClass.GetMouseWorldPosition();
            grid.GetCoordinates(mouseWorldPosition, out int x, out int y);
            PathNode currentNode = grid.GetCell(x, y);

            if (currentNode.hasRobot)
            {
                foreach (RobotsBrain r in robotList)
                {
                    if (Mathf.RoundToInt((r.transform.position.x - 5) / 10f) == currentNode.x && Mathf.RoundToInt((r.transform.position.y - 5) / 10f) == currentNode.y)
                    {
                        draggedObject = r.gameObject;
                        draggedRobot = r;
                        Destroy(draggedObject);
                        currentNode.SetRobot(false);
                        currentNode.SetIsWalkable(true);

                    }
                }

                robotList.Remove(draggedRobot);
            }
            else if (currentNode.hasStation)
            {
                foreach (Station s in stationtList)
                {
                    if (Mathf.RoundToInt((s.transform.position.x - 5) / 10f) == currentNode.x && Mathf.RoundToInt((s.transform.position.y - 5) / 10f) == currentNode.y)
                    {
                        draggedObject = s.gameObject;
                        draggedStation = s;
                        Destroy(draggedObject);
                        Station.currentIndex--;
                        currentNode.SetStation(false);
                    }
                }
                stationtList.Remove(draggedStation);
            }
            else if (x == 0 || y == 0 || x == grid.GetWidth() - 1 || y == grid.GetHeight() - 1)
            {
                CreateStation(x, y);
            }
            else
            {
                CreateRobot(x, y);
                capacityButton.interactable = true;
            }
        }


    }

    #region Methods
    // Run the robots (Run button)
    public void RunRobots()
    {
        collectionStartegy.Startegy(orders, robotList, stationtList);
        
        orders.Clear();
        mousePositionsOrders.Clear();
    }

    // Remove the items from the Grid after Add Order is clicked
    public void RemoveItemsFromGrid()
    {
        for (int i = 0; i < grid.GetWidth(); i++)
        {
            for (int j = 0; j < grid.GetHeight(); j++)
            {
                if (grid.GetCell(i, j).isItem)
                {
                    grid.GetCell(i, j).SetIsItem(!grid.GetCell(i, j).isItem);
                    mousePositionsOrders.Clear();

                }
            }
        }
    }

    public void OpenCapacityWindow()
    {
        capacityInputWindow.show();
    }

    public void EnterCapacity()
    {
        capacity = Int32.Parse(capacityInputWindow.inputField.text);

        if (capacity > 0)
        {
            capacityButton.GetComponent<Image>().color = Color.green;
            capacityText.text = capacity.ToString();

        }

        foreach (RobotsBrain rb in robotList)
            rb.SetCapacity(capacity);
        
        capacityInputWindow.Hide();
    }

    public void CancelCapacityWindow()
    {
        capacityInputWindow.Hide();
    }
    
    public void AddOrder()
    {
        List<Vector3> order = new List<Vector3>();

        foreach (Vector3 item in mousePositionsOrders)
        {
            order.Add(item);
        }

        orders.Add(order);
        mousePositionsOrders.Clear();

        //   Clear();
    }

    public void Clear()
    {
        for (int i = 0; i < grid.GetWidth(); i++)
        {
            for (int j = 0; j < grid.GetHeight(); j++)
            {
                if (grid.GetCell(i, j).isItem)
                {
                    grid.GetCell(i, j).SetIsItem(!grid.GetCell(i, j).isItem);
                    mousePositionsOrders.Clear();
                    

                }
                
            }
        }

        orders.Clear();
    }
    
    public void Positions()
    {
        for (int i = 0; i < grid.GetWidth(); i++)
        {
            for (int j = 0; j < grid.GetHeight(); j++)
            {
                if (grid.GetCell(i, j).ToString() == "")
                {
                    grid.GetCell(i, j).SetNodeUI("(" + i + ", " + j + ")");
                    grid.TriggerCell(i, j);
                }
                else
                {
                    grid.GetCell(i, j).SetNodeUI("");
                    grid.TriggerCell(i, j);
                }

            }
        }
    }
    
    public void OpenSaveWindow()
    {
        saveInputWindow.show();
    }

    public void SceneReturn()
    {
        SceneManager.LoadScene(0);
    }

    public void ResetScene()
    {
        Station.currentIndex = 0;

        SceneManager.LoadScene("Human Robot Collaboration");   
    }

    public void CancelSaveWindow()
    {
        saveInputWindow.Hide();
    }

    public void SaveLayout()
    {
        sqLiteManager.Save(grid, saveInputWindow.inputField.text);
        saveInputWindow.Hide();
    }

    // Get the list of all collection strategies
    public List<CollectionStrategies> GetInitializedCollectionStartegies()
    {
        List<CollectionStrategies> output = new List<CollectionStrategies>();
        try
        {
            output = AppDomain.CurrentDomain.GetAssemblies().
                SelectMany(x => x.GetTypes()).Where(x => typeof(CollectionStrategies).
                IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).
                Select(x => (CollectionStrategies)Activator.CreateInstance(x, grid)).ToList();
        }
        catch
        {
            Debug.LogWarning("One of the custom algorithms does not have the option to be instanciated with a default constructor");
        }
        return output;
    }

    // Get the list of all Robots' brains
    public List<Type> GetInitializedRobotBrainAlgorithms()
    {
        List<Type> output = new List<Type>();
        try
        {
            output = AppDomain.CurrentDomain.GetAssemblies().
                SelectMany(x => x.GetTypes()).Where(x => typeof(RobotsBrain).
                IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).ToList();
        }
        catch
        {
            Debug.LogWarning("An unexpected error occured, please check.");
        }
        return output;
    }

    // Spawn a Robot in the position x and y
    public void CreateRobot(int x, int y)
    {
        GameObject newRobot = GameObject.Instantiate(robotPrefab, instancesParent);
        newRobot.transform.position = new Vector3(x * 10 + 5, y * 10 + 5, 0);
        robotSelection.allRobots.Add(newRobot);
        robotSelection.AddRobotComponent(newRobot);
        grid.GetCell(x, y).SetRobot(true);
    }

    // Spawn a Station in the position x and y
    public void CreateStation(int x, int y)
    {
        GameObject newStation = GameObject.Instantiate(stationPrefab, instancesParent);
        newStation.transform.position = new Vector3(x * 10 + 5, y * 10 + 5, 0);
        newStation.transform.GetChild(0).gameObject.SetActive(false);
        stationtList.Add(newStation.GetComponent<Station>());
        grid.GetCell(x,y).SetStation(true);
    }

    public Grid<PathNode> GetGrid()
    {
        return this.grid;
    }
    #endregion
}