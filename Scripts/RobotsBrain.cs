using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RobotsBrain : MonoBehaviour
{
    protected Grid<PathNode> grid;
    protected int capacity;
   
    // Initialize Robots
    public virtual void SetGrid(Grid<PathNode> grid)
    {this.grid = grid;}

    public virtual void SetCapacity(int capacity)
    {this.capacity = capacity;}

    // The chosen name will be shown in dropdown menu.
    public abstract string GetName();
    
    // This method is only called once for a given object
    public abstract void Start();

    // This method is called once per frame
    public abstract void Update();
    
}
