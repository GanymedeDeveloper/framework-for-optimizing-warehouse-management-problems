using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SavingsAlgorithm : OrderBatchingAlgorithms
{
    #region Attributes
    private List<Batch> batchList = new List<Batch>();

    private List<(List<Vector3>, List<Vector3>)> pairs = new List<(List<Vector3>, List<Vector3>)>();
    private Dictionary<int, int> savings = new Dictionary<int, int>();
    private List<Vector3> mergedOrders = new List<Vector3>();

    private List<PathNode> order1 = new List<PathNode>();
    private List<PathNode> order2 = new List<PathNode>();
    private List<PathNode> order1And2 = new List<PathNode>();

    private List<Vector3> order1V = new List<Vector3>();
    private List<Vector3> order2V = new List<Vector3>();
    private List<Vector3> order1And2V = new List<Vector3>();

    private Dictionary<int, Batch> assignedOrders = new Dictionary<int, Batch>();
    private List<List<Vector3>> orders = new List<List<Vector3>>();
    #endregion

    public SavingsAlgorithm(Grid<PathNode> grid) : base(grid)
    {

    }

    public override string GetName()
    {
        return "Savings Algorithm";
    }

    public override List<Batch> BatchOrders(List<List<Vector3>> ordersList, int pickDeviceCapacity, PickPathFindingAlgorithms pathfinding)
    {
        orders = ordersList;

        // Make pairs of Orders
        pairs = MakeOrderPairs(ordersList);

        // Calculate The Savings matrix of each Pair
        for (int i = 0; i < pairs.Count; i++)
        {
            if (pairs[i].Item1.Count + pairs[i].Item2.Count > pickDeviceCapacity)
            {
                savings.Add(i, -1);
            }

            else
            {
                foreach (Vector3 item in pairs[i].Item1) { mergedOrders.Add(item); }
                foreach (Vector3 item in pairs[i].Item2) { mergedOrders.Add(item); }

                // Find a path to collect an order using the NN&Dijkstra
                order1V = pathfinding.FindPath(pairs[i].Item1);

                // Get PathNodes out of Vector3
                order1 = ReturnPathNodeList(order1V);
                order2V = pathfinding.FindPath(pairs[i].Item2);
                order2 = ReturnPathNodeList(order2V);
                order1And2V = pathfinding.FindPath(mergedOrders);
                order1And2 = ReturnPathNodeList(order1And2V);

                // Calculate the number of cells covered by the route
                int timeOrder1Collection = CalculateCollectingTime(order1);

                int timeOrder2Collection = CalculateCollectingTime(order2);
                int timeBothCollection = CalculateCollectingTime(order1And2);

                savings.Add(i, timeOrder1Collection + timeOrder2Collection - timeBothCollection);

                mergedOrders.Clear();
            }
        }

        foreach (KeyValuePair<int, int> kvp in savings.OrderByDescending(key => key.Value))
        {
            if ((!IsAssigned(pairs[kvp.Key].Item1, assignedOrders) && !IsAssigned(pairs[kvp.Key].Item2, assignedOrders)) && (pairs[kvp.Key].Item1.Count + pairs[kvp.Key].Item2.Count <= pickDeviceCapacity))
            {
                Batch batch = new Batch();
                batch.AddOrder(pairs[kvp.Key].Item1);
                batch.AddOrder(pairs[kvp.Key].Item2);
                assignedOrders.Add(orders.IndexOf(pairs[kvp.Key].Item1), batch);
                assignedOrders.Add(orders.IndexOf(pairs[kvp.Key].Item2), batch);
                batchList.Add(batch);
            }

            else if (IsAssigned(pairs[kvp.Key].Item1, assignedOrders) ^ IsAssigned(pairs[kvp.Key].Item2, assignedOrders))
            {
                if (IsAssigned(pairs[kvp.Key].Item1, assignedOrders))
                {
                    Batch wantedBatch = FindBatch(pairs[kvp.Key].Item1, assignedOrders);

                    if (wantedBatch.BatchSize() + pairs[kvp.Key].Item2.Count <= pickDeviceCapacity)
                    {
                        wantedBatch.AddOrder(pairs[kvp.Key].Item2);
                        assignedOrders.Add(orders.IndexOf(pairs[kvp.Key].Item2), wantedBatch);
                    }

                }

                else if (IsAssigned(pairs[kvp.Key].Item2, assignedOrders))
                {
                    Batch wantedBatch = FindBatch(pairs[kvp.Key].Item2, assignedOrders);

                    if (wantedBatch.BatchSize() + pairs[kvp.Key].Item1.Count <= pickDeviceCapacity)
                    {
                        wantedBatch.AddOrder(pairs[kvp.Key].Item1);
                        assignedOrders.Add(orders.IndexOf(pairs[kvp.Key].Item1), wantedBatch);
                    }

                }

                else { }

            }

            else
            {

            }
        }

        foreach (List<Vector3> order in orders)
        {
            if (!IsAssigned(order, assignedOrders))
            {
                Batch lastBatches = new Batch();
                lastBatches.AddOrder(order);

                batchList.Add(lastBatches);
            }
        }

        return batchList;
    }
    
    #region Help Methods

    // Get the number of cells covered by a route
    public int CalculateCollectingTime(List<PathNode> order)
    {
        int p = Mathf.Abs(0 - order[0].x);
        int q = Mathf.Abs(0 - order[0].y);
        int s = p + q;

        for (int i = 0; i < order.Count - 1; i++)
        {
            p = Mathf.Abs(order[i].x - order[i + 1].x);
            q = Mathf.Abs(order[i].y - order[i + 1].y);
            s = s + p + q;

        }

        return s;
    }
    
    private List<PathNode> ReturnPathNodeList(List<Vector3> VectorList)
    {
        List<PathNode> pn = new List<PathNode>();

        foreach (Vector3 v in VectorList)
        {
            pn.Add(new PathNode(grid, (int)v.x, (int)v.y));

        }

        return pn;
    }

    public List<(List<Vector3>, List<Vector3>)> MakeOrderPairs(List<List<Vector3>> ordersList)
    {
        List<(List<Vector3>, List<Vector3>)> pairs = new List<(List<Vector3>, List<Vector3>)>();

        // Making a List of order pairs
        for (int i = 0; i < ordersList.Count; i++)
        {
            for (int j = 0; j < ordersList.Count; j++)
            {
                if (i == j) { continue; }

                pairs.Add((ordersList[i], ordersList[j]));
            }
        }

        // Delete dupliate pairs
        for (int i = 0; i < ordersList.Count; i++)
        {
            for (int j = 0; j < ordersList.Count; j++)
            {
                if (pairs.Contains((ordersList[i], ordersList[j])))
                {
                    pairs.Remove((ordersList[j], ordersList[i]));
                }
            }
        }

        return pairs;
    }

    // Check whether an order is assigned to a Batch
    public bool IsAssigned(List<Vector3> order, Dictionary<int, Batch> assigned)
    {
        bool wanted = false;

        foreach (int k in assigned.Keys)
        {
            if (orders[k] == order)
            {
                wanted = true;
            }

        }

        return wanted;
    }

    // Find the corresponding Batch of an order
    public Batch FindBatch(List<Vector3> order, Dictionary<int, Batch> assigned)
    {
        Batch wanted = new Batch();

        foreach (KeyValuePair<int, Batch> b in assigned)
        {
            if (b.Key == orders.IndexOf(order))
            {
                wanted = b.Value;
            }
        }

        return wanted;
    }
    #endregion

}





