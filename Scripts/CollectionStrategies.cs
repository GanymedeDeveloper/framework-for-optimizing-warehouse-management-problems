using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CollectionStrategies 
{
    protected Grid<PathNode> grid;

    public CollectionStrategies(Grid<PathNode> grid)
    { this.grid = grid; }

    // The chosen name will be shown in dropdown menu.
    public abstract string GetName();

    // Implement your collection strategy.
    public abstract void Startegy(List<List<Vector3>> orders, List<RobotsBrain> robots, List<Station> stations);
}
