
using CodeMonkey.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OrderBatchingManager : MonoBehaviour
{
    #region Attributes
    [SerializeField] private LayoutInputWindow inputWindow;
    [SerializeField] private InputWindow saveInputWindow;
    [SerializeField] private SQLiteManager sqLiteManager;
    [SerializeField] private Transform pfPathfindingDebugStepVisualNode;

    [SerializeField] private GridVisuals pathfindingVisual;
    [SerializeField] private Button Capacitybutton;

    [SerializeField] private TextMeshProUGUI CapacityText;
    [SerializeField] public Button BatchButton;

    private HashSet<Vector3> mousePositionsOrders;
    public List<List<Vector3>> orders;
    private List<Batch> batchedOrderList;

    public PickPathFindingAlgorithms pathFinding;
    public OrderBatchingAlgorithms orderBatchingaAlgorithm;
    
    private Grid<PathNode> grid;
    private int capacity;
    private int sceneIndex;
    #endregion

    private void Awake()
    {
        if (ChooseDropDown.mainGrid != null)
        {
            grid = new Grid<PathNode>(ChooseDropDown.mainGrid.GetWidth(), ChooseDropDown.mainGrid.GetHeight(), 10f, (Grid<PathNode> g, int x, int y) => new PathNode(g, x, y));

            for (int i = 0; i < ChooseDropDown.mainGrid.GetWidth(); i++)
            {
                for (int j = 0; j < ChooseDropDown.mainGrid.GetHeight(); j++)
                {
                    PathNode pn = new PathNode(grid, i, j);
                    pn.SetIsItem(ChooseDropDown.mainGrid.GetCell(i, j).isItem);
                    pn.SetIsWalkable(ChooseDropDown.mainGrid.GetCell(i, j).isWalkAble);

                    grid.SetCell(i, j, pn);
                }
            }
        }
        else
        {
            grid = new Grid<PathNode>(20, 10, 10f, (Grid<PathNode> g, int x, int y) => new PathNode(g, x, y));
        }
        
    }

    private void Start()
    {
        mousePositionsOrders = new HashSet<Vector3>();
        orders = new List<List<Vector3>>();
        pathfindingVisual.SetGrid(grid);
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        RemoveItemsFromGrid();

        BatchButton.interactable = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            for (int i = 0; i < grid.GetWidth(); i++)
            {
                for (int j = 0; j < grid.GetHeight(); j++)
                {
                    grid.GetCell(i, j).SetNodeUI("(" + i + " " + j + ")");
                    grid.TriggerCell(i, j);
                }
            }
        }

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            Vector3 mouseWorldPosition = UtilsClass.GetMouseWorldPosition();
            grid.GetCoordinates(mouseWorldPosition, out int x, out int y);

            if (grid.GetCell(x, y) != null)
            {
                grid.GetCell(x, y).SetIsItem(!grid.GetCell(x, y).isItem);

                if (!mousePositionsOrders.Contains(grid.GetMouseCoordinates(x, y)))
                {
                    mousePositionsOrders.Add(grid.GetMouseCoordinates(x, y));
                }
                else
                {
                    mousePositionsOrders.Remove(grid.GetMouseCoordinates(x, y));
                }

            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            Vector3 mouseWorldPosition = UtilsClass.GetMouseWorldPosition();
            grid.GetCoordinates(mouseWorldPosition, out int x, out int y);
            grid.GetCell(x, y).SetIsWalkable(!grid.GetCell(x, y).isWalkAble);
        }

        if (Input.GetMouseButtonDown(2))
        {
            Vector3 mouseWorldPosition = UtilsClass.GetMouseWorldPosition();
            grid.GetCoordinates(mouseWorldPosition, out int x, out int y);

            if (x == 0)
            {
                Vector3 gridPosition = new Vector3(x * 10, y * 10) + Vector3.one * grid.GetCellSize() * .5f;
                Transform visualNode = CreateVisualNode(gridPosition);

                visualNode.Find("sprite").GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
            }
            else if (y == 1)
            {
                Vector3 gridPosition = new Vector3(x * 10, y * 10) + Vector3.one * grid.GetCellSize() * .5f;
                Transform visualNode = CreateVisualNode(gridPosition);

                visualNode.Find("sprite").GetComponent<SpriteRenderer>().color = new Color(0, 1, 0);
            }
            else
            {
                Vector3 gridPosition = new Vector3(x * 10, y * 10) + Vector3.one * grid.GetCellSize() * .5f;
                Transform visualNode = CreateVisualNode(gridPosition);

                visualNode.Find("sprite").GetComponent<SpriteRenderer>().color = new Color(0, 0, 1);
            }
        }


    }
    
    #region Methods
    // Show the coordinates of each cell on the grid (Positions button)
    public void Positions()
    {
        for (int i = 0; i < grid.GetWidth(); i++)
        {
            for (int j = 0; j < grid.GetHeight(); j++)
            {
                if (grid.GetCell(i, j).ToString() == "")
                {
                    grid.GetCell(i, j).SetNodeUI("(" + i + ", " + j + ")");
                    grid.TriggerCell(i, j);
                }
                else
                {
                    grid.GetCell(i, j).SetNodeUI("");
                    grid.TriggerCell(i, j);
                }

            }
        }
    }

    // Batch the orders (Batch button)
    public void Batch()
    {
       // batchedOrderList = orderBatching2.BatchOrders(orders, 4, pathfinding);
        batchedOrderList = orderBatchingaAlgorithm.BatchOrders(orders, capacity, pathFinding);

        int counter = 1;
        var colors = GetStaticPropertyBag(typeof(Color));
        colors.Remove("black");
        foreach (Batch batch in batchedOrderList)
        {
            //Debug.Log(x);
            foreach (Vector3 item in batch.GetBatchList())
            {
                PathNode n = new PathNode(grid, (int)item.x , (int)item.y );

                Vector3 gridPosition = new Vector3(item.x, item.y) + Vector3.one * grid.GetCellSize() * .5f;
                Transform visualNode = CreateVisualNode(gridPosition);
                
                visualNode.Find("sprite").GetComponent<SpriteRenderer>().color = (Color)colors.Values.First();
                
                n.SetNodeUI("Batch" + counter.ToString());
                grid.SetCell(item, n);
              
            }
            colors.Remove(colors.Keys.First());
            counter++;
        }
        
    }

    // Get a map of Colors
    public static Dictionary<string, object> GetStaticPropertyBag(Type t)
    {
        const BindingFlags flags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

        var map = new Dictionary<string, object>();
        foreach (var prop in t.GetProperties(flags))
        {
            map[prop.Name] = prop.GetValue(null, null);
        }
        return map;
    }

    // Get a list of all Pick Path algorithms
    public List<PickPathFindingAlgorithms> GetInitializedPFAlgorithms()
    {
        List<PickPathFindingAlgorithms> output = new List<PickPathFindingAlgorithms>();
        try
        {
            output = AppDomain.CurrentDomain.GetAssemblies().
                SelectMany(x => x.GetTypes()).Where(x => typeof(PickPathFindingAlgorithms).
                IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).
                Select(x => (PickPathFindingAlgorithms)Activator.CreateInstance(x, grid)).ToList();
        }
        catch
        {
            Debug.LogWarning("One of the custom algorithms does not have the option to be instanciated with a default constructor");
        }
        return output;
    }

    // Get a list of all Order Batching algorithms
    public List<OrderBatchingAlgorithms> GetInitializedAlgorithms()
    {
        List<OrderBatchingAlgorithms> output = new List<OrderBatchingAlgorithms>();
        try
        {
            output = AppDomain.CurrentDomain.GetAssemblies().
                SelectMany(x => x.GetTypes()).Where(x => typeof(OrderBatchingAlgorithms).
                IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).
                Select(x => (OrderBatchingAlgorithms)Activator.CreateInstance(x, grid)).ToList();
        }
        catch
        {
            Debug.LogWarning("One of the custom algorithms does not have the option to be instanciated with a default constructor");
        }
        return output;
    }

    // Return to Main Menu (Return Button)
    public void SceneReturn()
    {
        SceneManager.LoadScene(sceneIndex - 3);
    }
    
    // Remove all items from the Grid
    public void RemoveItemsFromGrid()
    {
        for (int i = 0; i < grid.GetWidth(); i++)
        {
            for (int j = 0; j < grid.GetHeight(); j++)
            {
                if (grid.GetCell(i, j).isItem)
                {
                    grid.GetCell(i, j).SetIsItem(!grid.GetCell(i, j).isItem);
                    mousePositionsOrders.Clear();
         
                }
            }
        }
    }
    
    // Clear the Orders list (Clear Button)
    public void Clear()
    {
        mousePositionsOrders.Clear();
        orders.Clear();
    }

    // Run a scan over the grid and make an order of the item cells (Add Order button)
    public void AddOrder()
    {
        List<Vector3> order = new List<Vector3>();
        
        foreach (Vector3 item in mousePositionsOrders)
        {
            order.Add(item);
        }

        orders.Add(order);

        RemoveItemsFromGrid();
    }
    
    // Open the Capacity Window (Capacity Button)
    public void OpenCapacityWindow()
    {
        inputWindow.show();
    }

    // Create a Grey Cell (Prefab)
    public Transform CreateVisualNode(Vector3 position)
    {
        Transform visualNodeTransform = UnityEngine.Object.Instantiate(pfPathfindingDebugStepVisualNode, position, Quaternion.identity) as Transform;
        return visualNodeTransform;
    }

    // Open the Save Window (Save Button)
    public void OpenSaveWindow()
    {
        saveInputWindow.show();
    }

    // Cancel the Capacity Window
    public void CancelCapacityWindow()
    {
        inputWindow.Hide();
    }

    // Cancel the Save Window
    public void CancelSaveWindow()
    {
        saveInputWindow.Hide();
    }

    // Save the Layout
    public void SaveLayout()
    {
        sqLiteManager.Save(grid, saveInputWindow.inputField.text);
        saveInputWindow.Hide();
    }

    // Reset the Scene (Reset Button)
    public void ResetScene()
    {
        SceneManager.LoadScene("OrderBatching");
    }
    
    public void EnterCapacity()
    {
        capacity = Int32.Parse(inputWindow.inputField.text);
        
        if (capacity > 1)
        {
            Capacitybutton.GetComponent<Image>().color = Color.green;
            CapacityText.text = capacity.ToString();
           
        }
        
        inputWindow.Hide();
    }
    #endregion
    
}
