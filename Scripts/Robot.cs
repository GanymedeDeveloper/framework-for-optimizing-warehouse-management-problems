using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Robot : RobotsBrain
{
    #region Attributes
    public Dictionary<Vector3, int> orderedItems;
    public Dictionary<Vector3, int> orderedItemsWayReturen;

    private List<Vector3> walkingPath;

    private Vector3 currentPos;
    private Vector3 nextPos;
    private Vector3 lastPos;
    public Vector3 shelfPosition;
    public Vector3 endPosition;

    private bool robotIsOnWayReturn;
    private bool robotMoveToShelf;

    private int itemCount;

    private float tickTime;
    private const float Time_Max = 0.3f;
    private int currentPathIndex;

    private List<PathNode> openList;
    private List<PathNode> closedList;

    protected const int MOVE_STRAIGHT_COST = 1;
    protected const int MOVE_DIAGONAL_COST = 2;
    #endregion

    public Robot()
    {

    }
    
    public override string GetName()
    {
        return "Robot";
    }

    public override void Start()
    {
        orderedItems = new Dictionary<Vector3, int>();
        orderedItemsWayReturen = new Dictionary<Vector3, int>();

        currentPos = transform.position;
        nextPos = transform.position;
        lastPos = transform.position;

        endPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        walkingPath = new List<Vector3>();

        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(false);
        transform.GetChild(4).gameObject.SetActive(false);
    }

    public override void Update()
    {
        // Get the next item or get out of Update
        if (walkingPath.Count == currentPathIndex)
        {
            if (orderedItems.Count > 0)
            {
                TakeNextItem();
            }

            grid.GetCell(transform.position).SetIsWalkable(false);
            grid.GetCell(transform.position).SetRobot(true);

            return;
        }

        tickTime += Time.deltaTime;

        if (tickTime >= Time_Max)
        {
            tickTime -= Time_Max;

            MoveAStep();
            
            // Check whether the robot is moving to shelf
            if (!robotMoveToShelf)
            {
                if (walkingPath.Count - currentPathIndex == 1)
                {
                    currentPathIndex++;

                    // Check wether the robot has to pick or to drop an item
                    if (!robotIsOnWayReturn)
                    {
                        PickUpItem();
                        capacity--;

                        // If there is still a capacity then fetch another item
                        if (capacity > 0 && orderedItems.Count > 0)
                        {
                            TakeNextItem();
                        }

                        // Calculate the way return to Station
                        else
                        {
                            robotIsOnWayReturn = true;
                            CalculateWayReturn();
                        }
                    }
                    // Drop the item
                    else
                    {
                        DropItem();
                        robotIsOnWayReturn = false;
                    }
                }
            }
            else
            {
                if (walkingPath.Count - currentPathIndex == 1)
                {
                    currentPathIndex++;
                }
            }
        }
        //InterpolateStep();
    }

    #region Methods
    // Define which item will be collected
    public void TakeNextItem()
    {
        grid.GetCell(orderedItems.Keys.First()).SetIsWalkable(true);
        walkingPath = FindPath(transform.position, orderedItems.Keys.First());

        orderedItemsWayReturen.Add(orderedItems.Keys.First(), orderedItems.Values.First());
        orderedItems.Remove(orderedItems.Keys.First());

        currentPathIndex = 0;
    }

    // This method is to interpolar the movement of the robot in the scene. 
    // It can be activated at the line 132 and the robots will move smoother in the scene
    public void InterpolateStep()
    {
        if (currentPathIndex < walkingPath.Count - 1)
        {
            currentPos = walkingPath[currentPathIndex];
            nextPos = walkingPath[currentPathIndex + 1];
            transform.position = Vector3.Lerp(currentPos, nextPos, Mathf.Clamp01(tickTime / Time_Max));

            grid.GetCell(nextPos).SetIsWalkable(false);
            grid.GetCell(nextPos).SetRobot(true);

            grid.GetCell(currentPos).SetIsWalkable(true);
            grid.GetCell(currentPos).SetRobot(false);
        }

        else
        {
            ReRoute();
        }
    }

    // Take one Step
    public void MoveAStep()
    {
        currentPos = walkingPath[currentPathIndex];
        nextPos = walkingPath[++currentPathIndex];

        if (grid.GetCell(nextPos).isWalkAble)
        {
            transform.position = nextPos;

            grid.GetCell(nextPos).SetIsWalkable(false);
            grid.GetCell(nextPos).SetRobot(true);

            grid.GetCell(currentPos).SetIsWalkable(true);
            grid.GetCell(currentPos).SetRobot(false);
        }
        else
        {
            ReRoute();
        }

    }

    // Reroute if there is a collision
    private void ReRoute()
    {
        if (FindPath(transform.position, walkingPath[walkingPath.Count - 1]) == null)
        {
            currentPathIndex--;
            return;
        }

        walkingPath = FindPath(transform.position, walkingPath[walkingPath.Count - 1]);
        currentPathIndex = 0;
    }

    // Pick the item
    public void PickUpItem()
    {
        grid.GetCoordinates(transform.position, out int x, out int y);
        grid.GetCell(x, y).SetIsItem(!grid.GetCell(x, y).isItem);
        transform.GetChild(0).gameObject.SetActive(true);
        itemCount++;
        transform.GetChild(1).GetComponent<TextMesh>().text = itemCount.ToString();
    }

    // Calculate the way back to Station
    public void CalculateWayReturn()
    {
        HashSet<Station> wantedStations = new HashSet<Station>();
        Dictionary<Vector3, int> deleteItems = new Dictionary<Vector3, int>();

        foreach (KeyValuePair<Vector3, int> knp in orderedItemsWayReturen)
        {
            wantedStations.Add(CollaborativeStrategy.stations.Find(x => x.GetIndex() == knp.Value));
            deleteItems.Add(knp.Key, knp.Value);

        }

        foreach (KeyValuePair<Vector3, int> knp in deleteItems)
        {
            orderedItemsWayReturen.Remove(knp.Key);
        }

        List<Vector3> path = new List<Vector3>();
        grid.GetCell(wantedStations.ElementAt(wantedStations.Count - 1).transform.position).SetIsWalkable(true);
        path = FindPath(transform.position, wantedStations.ElementAt(wantedStations.Count - 1).transform.position);

        if (path == null)
        {
            transform.GetChild(2).gameObject.SetActive(true);
        }

        walkingPath.Clear();
        currentPathIndex = 0;

        foreach (Vector3 v in path)
        {
            walkingPath.Add(v);
        }

        if (wantedStations.Count > 1)
        {
            for (int i = wantedStations.Count - 1; i > 0; i--)
            {
                grid.GetCell(wantedStations.ElementAt(i - 1).transform.position).SetIsWalkable(true);
                path = FindPath(wantedStations.ElementAt(i).transform.position, wantedStations.ElementAt(i - 1).transform.position);

                for (int j = 1; j < path.Count; j++)
                {
                    walkingPath.Add(path[j]);
                }

            }
        }


    }

    // Drop the item in the Station
    public void DropItem()
    {
        grid.GetCoordinates(transform.position, out int x, out int y);
        transform.GetChild(0).gameObject.SetActive(false);

        //privateCapacity = capacity;
        capacity = itemCount;
        itemCount = 0;

        if (orderedItems.Count == 0)
        {
            shelfPosition = CalculateShelfPosition();
            robotMoveToShelf = true;
            MoveToShelf();
        }

        transform.GetChild(1).GetComponent<TextMesh>().text = itemCount.ToString();
        transform.GetChild(1).GetComponent<TextMesh>().text = itemCount == 0 ? "" : itemCount.ToString();
    }

    // This method will be called once the robot have finished collecting the items
    // The robot will move to a shelf to aovid further collisions near the station
    // Given that different robots might collect the same order for the same Station 
    public void MoveToShelf()
    {
        walkingPath = /*pathfinding.*/FindPath(transform.position, shelfPosition);
        currentPathIndex = 0;
    }

    // Calculate a way to a shelf
    public Vector3 CalculateShelfPosition()
    {
        Vector3 wanted = new Vector3();
        Vector3 shelfP = new Vector3();
        if (((endPosition.x + 50) / 10) <= grid.GetWidth() - 1)
        {
            endPosition = new Vector3(endPosition.x + 50, endPosition.y, endPosition.z);
            shelfP = endPosition;
        }
        else if (((endPosition.y + 50) / 10) <= grid.GetHeight() - 1)
        {
            endPosition = new Vector3(endPosition.x, endPosition.y + 50, endPosition.z);
            shelfP = endPosition;
        }
        else
        {
            endPosition = new Vector3(endPosition.x, endPosition.y - 50, endPosition.z);
            shelfP = endPosition;
        }

        PathNode shelfN = grid.GetCell((int)shelfP.x / 10, (int)shelfP.y / 10);

        if (shelfN.isWalkAble)
        {
            wanted = shelfP;
        }
        else
        {
            List<PathNode> nearNodes = GetNeighbourList(shelfN);

            foreach (PathNode pn in nearNodes)
            {
                if (pn.isWalkAble)
                {
                    wanted = grid.GetMouseCoordinates(pn.x, pn.y);
                    break;
                }
            }
        }

        return wanted;
    }
    #endregion

    #region Dijkstra
    // Dijkstra
    public List<PathNode> FindPath(int startX, int startY, int endX, int endY)
    {
        PathNode startNode = grid.GetCell(startX, startY);
        PathNode endNode = grid.GetCell(endX, endY);

        if (startNode == null || endNode == null)
        {
            // Invalid Path
            return null;
        }

        openList = new List<PathNode> { startNode };
        closedList = new List<PathNode>();

        for (int x = 0; x < grid.GetWidth(); x++)
        {
            for (int y = 0; y < grid.GetHeight(); y++)
            {
                PathNode pathNode = grid.GetCell(x, y);
                pathNode.gCost = 99999999;
                pathNode.CalculateFCost();
                pathNode.cameFromNode = null;
            }
        }

        startNode.gCost = 0;
        startNode.hCost = CalculateDistanceCost(startNode, endNode);
        startNode.CalculateFCost();

        
        while (openList.Count > 0)
        {
            PathNode currentNode = GetLowestFCostNode(openList);
            if (currentNode == endNode)
            {
                
                return CalculatePath(endNode);
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);

            foreach (PathNode neighbourNode in GetNeighbourList(currentNode))
            {
                if (closedList.Contains(neighbourNode)) continue;
                if (!neighbourNode.isWalkAble)
                {
                    closedList.Add(neighbourNode);
                    continue;
                }

                int tentativeGCost = currentNode.gCost + CalculateDistanceCost(currentNode, neighbourNode);
                if (tentativeGCost < neighbourNode.gCost)
                {
                    neighbourNode.cameFromNode = currentNode;
                    neighbourNode.gCost = tentativeGCost;
                    neighbourNode.hCost = CalculateDistanceCost(neighbourNode, endNode);
                    neighbourNode.CalculateFCost();

                    if (!openList.Contains(neighbourNode))
                    {
                        openList.Add(neighbourNode);
                    }
                }
                
            }
        }

        // Out of nodes on the openList
        return null;
    }

    public List<Vector3> FindPath(Vector3 startWorldPosition, Vector3 endWorldPosition)
    {
        grid.GetCoordinates(startWorldPosition, out int startX, out int startY);
        grid.GetCoordinates(endWorldPosition, out int endX, out int endY);

        List<PathNode> path = FindPath(startX, startY, endX, endY);
        if (path == null)
        {
            return null;
        }
        else
        {
            List<Vector3> vectorPath = new List<Vector3>();
            foreach (PathNode pathNode in path)
            {
                vectorPath.Add(new Vector3(pathNode.x, pathNode.y) * grid.GetCellSize() + Vector3.one * grid.GetCellSize() * .5f);
            }
            return vectorPath;
        }
    }

    public PathNode GetLowestFCostNode(List<PathNode> pathNodeList)
    {
        PathNode lowestFCostNode = pathNodeList[0];
        for (int i = 1; i < pathNodeList.Count; i++)
        {
            if (pathNodeList[i].fCost < lowestFCostNode.fCost)
            {
                lowestFCostNode = pathNodeList[i];
            }
        }
        return lowestFCostNode;
    }

    public List<PathNode> GetNeighbourList(PathNode currentNode)
    {
        List<PathNode> neighbourList = new List<PathNode>();

        if (currentNode.x - 1 >= 0)
        {
            // Left
            neighbourList.Add(GetNode(currentNode.x - 1, currentNode.y));
            // Left Down
            if (currentNode.y - 1 >= 0) neighbourList.Add(GetNode(currentNode.x - 1, currentNode.y - 1));
            // Left Up
            if (currentNode.y + 1 < grid.GetHeight()) neighbourList.Add(GetNode(currentNode.x - 1, currentNode.y + 1));
        }
        if (currentNode.x + 1 < grid.GetWidth())
        {
            // Right
            neighbourList.Add(GetNode(currentNode.x + 1, currentNode.y));
            // Right Down
            if (currentNode.y - 1 >= 0) neighbourList.Add(GetNode(currentNode.x + 1, currentNode.y - 1));
            // Right Up
            if (currentNode.y + 1 < grid.GetHeight()) neighbourList.Add(GetNode(currentNode.x + 1, currentNode.y + 1));
        }
        // Down
        if (currentNode.y - 1 >= 0) neighbourList.Add(GetNode(currentNode.x, currentNode.y - 1));
        // Up
        if (currentNode.y + 1 < grid.GetHeight()) neighbourList.Add(GetNode(currentNode.x, currentNode.y + 1));

        return neighbourList;
    }

    public List<PathNode> CalculatePath(PathNode endNode)
    {
        List<PathNode> path = new List<PathNode>();
        path.Add(endNode);
        PathNode currentNode = endNode;
        while (currentNode.cameFromNode != null)
        {
            path.Add(currentNode.cameFromNode);
            currentNode = currentNode.cameFromNode;
        }
        path.Reverse();
        return path;
    }

    public int Distance(int x1, int x2, int y1, int y2)
    {
        return Math.Abs(x1 - y1) + Math.Abs(x2 - y2);
    }

    public int CalculateDistanceCost(PathNode a, PathNode b)
    {
        int xDistance = Mathf.Abs(a.x - b.x);
        int yDistance = Mathf.Abs(a.y - b.y);
        int remaining = Mathf.Abs(xDistance - yDistance);
        return MOVE_DIAGONAL_COST * Mathf.Min(xDistance, yDistance) + MOVE_STRAIGHT_COST * remaining;
    }

    public PathNode GetNode(int x, int y)
    {
        return grid.GetCell(x, y);
    }

    protected Grid<PathNode> GetGrid()
    {
        return grid;
    }

    #endregion
}
