using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Batch
{
    private List<Vector3> orders = new List<Vector3>();

    // Add an order to the batch
    public void AddOrder(List<Vector3> order)
    {
        foreach (Vector3 item in order) orders.Add(item);  
    }

    // Check whether the Batch contain the given order
    public bool ContainOrder(List<Vector3> order)
    {
        return !order.Except(orders).Any();
    }

    // Returns the number of items in the Batch
    public int BatchSize()
    {
        return orders.Count;
    }

    // Get the items from the Batch
    public List<Vector3> GetBatchList()
    {
        return orders;
    }
}

