using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;

public class SQLiteManager : MonoBehaviour
{
    
    private void Start()
    {
        
    }

    // Get the names of the stored warehouse layouts in the Database
    public List<string> LoadNames()
    {
        List<string> gridName = new List<string>();
        string conn = "URI=file:" + Application.dataPath + "/DataBase.sqlite";
        IDbConnection dbconn;
        dbconn = new SqliteConnection(conn);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();

        string sqlQuery = "SELECT * FROM sqlite_master where type='table'";

        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();

        while (reader.Read())
        {
            gridName.Add(reader[1].ToString());
        }

        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

        return gridName;
    }

    // Get the warehouse layout with the given name from the Database
    public Grid<PathNode> LoadGrid(string gridName)
    {
        string conn = "URI=file:" + Application.dataPath + "/DataBase.sqlite";
        IDbConnection dbconn;
        dbconn = new SqliteConnection(conn);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery;

         sqlQuery = "SELECT Width, Height FROM " + gridName;

        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
   
         int width  = Convert.ToInt32(reader[0]);
         int height = Convert.ToInt32(reader[1]);
        
        Grid<PathNode> grid = new Grid<PathNode>(width, height, 10f, (Grid<PathNode> g, int x, int y) => new PathNode(g, x, y));

        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

        IDbConnection dbconn2;
        dbconn2 = new SqliteConnection(conn);
        dbconn2.Open();
        IDbCommand dbcmd2 = dbconn2.CreateCommand();

        sqlQuery = "SELECT X, Y, IsWalkable, IsItem FROM " + gridName;
        dbcmd2.CommandText = sqlQuery;

        IDataReader reader2 = dbcmd2.ExecuteReader();

        while (reader2.Read())
        {
            PathNode node = new PathNode(grid, Convert.ToInt32(reader2[0]), Convert.ToInt32(reader2[1]));

            node.SetIsWalkable(Convert.ToInt32(reader2[2]) == 1 ? true  : false);
            node.SetIsItem(Convert.ToInt32(reader2[3]) == 0 ? true : false);

            grid.SetCell(Convert.ToInt32(reader2[0]), Convert.ToInt32(reader2[1]), node);
        }

        return grid;
    }

    // Delete the warehouse layout with the given name from the Database
    public void Delete(string gridName)
    {
        string conn = "URI=file:" + Application.dataPath + "/DataBase.sqlite";
        IDbConnection dbconn;
        dbconn = new SqliteConnection(conn);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();

        string sqlQuery = "DROP TABLE " + gridName;

        dbcmd.CommandText = sqlQuery;
        dbcmd.ExecuteNonQuery();

        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

    }
    
    // Store the warehouse layout with the given name into the Database 
    public void Save(Grid<PathNode> grid, string gridName)
    {
        string conn = "URI=file:" + Application.dataPath + "/DataBase.sqlite";
        IDbConnection dbconn;
        dbconn = new SqliteConnection(conn);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();

        string sqlQuery =
            "create table " + gridName + " ( " +
            " ID Integer, " +
            " X  Integer, " +
            " Y  Integer, " +
            " IsWalkable Integer, " +
            " IsItem     Integer, " +
            " Width Integer, " +
            " Height Integer " + " );";

        dbcmd.CommandText = sqlQuery;
        dbcmd.ExecuteNonQuery();
        
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

        InstertEntries(grid, gridName);
    }
    
    public void InstertEntries(Grid<PathNode> grid, string gridName)
    {
        string conn = "URI=file:" + Application.dataPath + "/DataBase.sqlite";
        IDbConnection dbconn;
        dbconn = new SqliteConnection(conn);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery;

        for (int i = 0; i < grid.GetWidth(); i++)
        {
            for (int j = 0; j < grid.GetHeight(); j++)
            {
                
                sqlQuery =
                "INSERT INTO " + gridName + "(ID, X, Y, IsWalkable, IsItem, Width, Height) " + "Values " +
                "( " + i * grid.GetHeight() + j + ", " +
                grid.GetCell(i, j).x + ", " +
                grid.GetCell(i, j).y + ", " +
                (grid.GetCell(i, j).isWalkAble ? 1 : 0) + ", " +
                (grid.GetCell(i, j).isItem ? 0 : 1) + ", " +
                 grid.GetWidth() + ", " +
                 grid.GetHeight()    +   " );";

                dbcmd.CommandText = sqlQuery;
                dbcmd.ExecuteNonQuery();

            }
        }

        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

}
