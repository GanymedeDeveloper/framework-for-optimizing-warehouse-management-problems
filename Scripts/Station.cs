using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Station : MonoBehaviour
{
    private int index;
    private List<Vector3> order = new List<Vector3>();

    public static int currentIndex = 0;

    private void Awake()
    {
        index = currentIndex++;
    }

    // Assign an Order to the Station
    public void AddOrder(List<Vector3> order)
    {
        this.order = new List<Vector3>(order);
    }

    // Check whether the Station is free
    public bool IsFree()
    {
        return order.Count == 0;
    }
    
    // Get the assigned Order
    public List<Vector3> GetOrder()
    {
        return order;
    }

    // Get Station's Index
    public int GetIndex()
    {
        return index;
    }
}




