using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

public class CollaborativeStrategy : CollectionStrategies
{
    public CollaborativeStrategy(Grid<PathNode> grid) : base(grid)
    {
    }

    public override string GetName()
    {
        return "Collaborative Strategy";
    }

    #region Attributes
    private List<List<Vector3>> orders;
    private Dictionary<Vector3,int> itemDictionary;
    private List<RobotsBrain> robots;
    private List<Vector3> foundItemsUsingLocalSearch;
    private Dictionary<Vector3, int> dicitonaryFoundItemsUsingLocalSearch;

    public static List<Station> stations = new List<Station>();
    #endregion

    public override void Startegy(List<List<Vector3>> orders, List<RobotsBrain> robots, List<Station> stations)
    {
        this.orders = new List<List<Vector3>>(orders);
        itemDictionary = new Dictionary<Vector3, int>();
        itemDictionary = new Dictionary<Vector3, int>();
        this.robots = robots;
        foundItemsUsingLocalSearch = new List<Vector3>();
        dicitonaryFoundItemsUsingLocalSearch = new Dictionary<Vector3, int>();
        
        InitStations(orders, stations);
        MakeMapOutOfOrders(stations);
        AssignAllItems(itemDictionary);
    }

    #region Methods
    public void InitStations (List<List<Vector3>> orders, List<Station> stations)
    {
        CollaborativeStrategy.stations = stations;

        AssignItemToStations();
    }

    public void AssignItemToStations()
    {
        while (orders.Count > 0)
        {
            for (int j = 0; j < stations.Count; j++)
            {
                if (orders.Count > 0)
                {
                    if (stations[j].IsFree())
                    {
                        stations[j].AddOrder(orders[0]);
                        CollaborativeStrategy.stations[j].transform.GetChild(0).gameObject.SetActive(true);
                        orders.Remove(orders[0]);
                    }
                    else
                    {
                        stations[j].AddOrder(stations[j].GetOrder().Concat(orders[0]).ToList());
                        orders.Remove(orders[0]);
                    }
                }
            }
        }
    }

    public Dictionary<Vector3, int> MakeMapOutOfOrders (List<Station> stations)
    {
        for (int i = 0; i < stations.Count; i++)
        {
            foreach (Vector3 item in stations[i].GetOrder())
            {
                itemDictionary.Add(item, stations[i].GetIndex());   
            }
        }
        
        return itemDictionary;
    }

    public void AssignAllItems (Dictionary<Vector3, int> items)
    {
        while (itemDictionary.Count > 0)
        {
            foreach (Robot robot in robots)
            {
                if (itemDictionary.Count != 0)
                {
                    KeyValuePair<Vector3, int> currentItem = AssignItem(robot);
                    robot.orderedItems.Add(currentItem.Key, currentItem.Value);

                    // Run a Local Search
                    foundItemsUsingLocalSearch = LocalSearch(currentItem.Key);

                    foreach (Vector3 item in foundItemsUsingLocalSearch)
                    {
                        itemDictionary.TryGetValue(item, out int result);
                        itemDictionary.Remove(item);
                        dicitonaryFoundItemsUsingLocalSearch.Add(item, result);
                    }
                    
                    foreach (KeyValuePair<Vector3, int> kvp in dicitonaryFoundItemsUsingLocalSearch)
                    {
                        robot.orderedItems.Add(kvp.Key, kvp.Value);
                    }

                    dicitonaryFoundItemsUsingLocalSearch.Clear();
                }
            }
            
        }
        
    }

    public KeyValuePair<Vector3, int> AssignItem(Robot robot)
    {
        KeyValuePair<Vector3, int> targetPos = NearestPoint3D((int)robot.transform.position.x, (int)robot.transform.position.y, itemDictionary);
        itemDictionary.Remove(targetPos.Key);

        return targetPos;
    }
    #endregion

    #region Finding Nearest Item
    public KeyValuePair<Vector3, int> NearestPoint3D(int x, int y, Dictionary<Vector3, int> points)
    {
        int dist;
        
        Vector3 closestPoint = points.First().Key;
        KeyValuePair<Vector3, int> wanted;

        int closestDist = Distance(x, y, (int)closestPoint.x, (int)closestPoint.y);

        // Traverse the array
        foreach (KeyValuePair<Vector3, int> kvp in points)
        {
            dist = Distance(x, y, (int)kvp.Key.x, (int)kvp.Key.y);

            if (dist < closestDist || dist == closestDist)
            {
                closestDist = dist;
                closestPoint = kvp.Key;
                wanted = kvp;
            }
        }

        return wanted;
    }

    public int Distance(int x1, int x2, int y1, int y2)
    {
        return Math.Abs(x1 - y1) + Math.Abs(x2 - y2);
    }
    #endregion
    
    #region Local Search
    public List<Vector3> LocalSearch(Vector3 item)
    {
        PathNode pn = grid.GetCell(item);
        List<PathNode> neighborList = GetNeighbourList(pn);

        List<Vector3> foundItems = new List<Vector3>();
        List<Vector3> neighborListPositions = new List<Vector3>();

        foreach (PathNode p in neighborList)
        {
            neighborListPositions.Add(new Vector3(p.x * 10, p.y * 10));
        }

        foreach (KeyValuePair<Vector3, int> kvp in itemDictionary)
        {
            if (neighborListPositions.Contains(kvp.Key))
            {
                foundItems.Add(kvp.Key);
            }
        }
        
        return foundItems;
    }

    public List<PathNode> GetNeighbourList(PathNode currentNode)
    {
        List<PathNode> neighbourList = new List<PathNode>();

        if (currentNode.x - 1 >= 0)
        {
            // Left
            neighbourList.Add(grid.GetCell(currentNode.x - 1, currentNode.y));
            // Left Down
            if (currentNode.y - 1 >= 0) neighbourList.Add(grid.GetCell(currentNode.x - 1, currentNode.y - 1));
            // Left Up
            if (currentNode.y + 1 < grid.GetHeight()) neighbourList.Add(grid.GetCell(currentNode.x - 1, currentNode.y + 1));
        }
        if (currentNode.x + 1 < grid.GetWidth())
        {
            // Right
            neighbourList.Add(grid.GetCell(currentNode.x + 1, currentNode.y));
            // Right Down
            if (currentNode.y - 1 >= 0) neighbourList.Add(grid.GetCell(currentNode.x + 1, currentNode.y - 1));
            // Right Up
            if (currentNode.y + 1 < grid.GetHeight()) neighbourList.Add(grid.GetCell(currentNode.x + 1, currentNode.y + 1));
        }
        // Down
        if (currentNode.y - 1 >= 0) neighbourList.Add(grid.GetCell(currentNode.x, currentNode.y - 1));
        // Up
        if (currentNode.y + 1 < grid.GetHeight()) neighbourList.Add(grid.GetCell(currentNode.x, currentNode.y + 1));

        return neighbourList;
    }
    #endregion
}
