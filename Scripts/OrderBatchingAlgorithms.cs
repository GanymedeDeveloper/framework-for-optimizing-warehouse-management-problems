using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OrderBatchingAlgorithms  
{
    protected Grid<PathNode> grid;

    public OrderBatchingAlgorithms(Grid<PathNode> grid)
    { this.grid = grid;}

    // Choose a name for your algorithm. The chosen name will be shown in dropdown
    public abstract string GetName();

    // Implement your Order Batching algorithm
    public abstract List<Batch> BatchOrders(List<List<Vector3>> ordersList, int pickDeviceCapacity, PickPathFindingAlgorithms pathfinding);
    
}
